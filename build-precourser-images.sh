#!/bin/bash
###############################################################################
#
# This Scipt builds the Dockerfile for 'RighteousCoder/openJDK-8'
#
# It logs the start and stop time and stdout & stderr into log.txt
#
###############################################################################

# Define a timestamp function
timestamp() {
  date +"%T"
}

#####################################################

cd "${0%/*}"

./android-sdk-image/build-23-sdk.sh
./android-sdk-image/build-23-avd.sh

