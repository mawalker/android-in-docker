package edu.vandy.mooc.rssreader.suite;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.model.RunnerScheduler;

import edu.vandy.mooc.rssreader.services.TestServiceTest;

/**
 * Created by Mike on 8/26/2016.
 */

// Runs all integration tests.
@RunWith(Suite.class)
@Suite.SuiteClasses({
        // comma separated list of classes that are test suites/integration tests.
        TestServiceTest.class
})
public class ServiceTests {
}
