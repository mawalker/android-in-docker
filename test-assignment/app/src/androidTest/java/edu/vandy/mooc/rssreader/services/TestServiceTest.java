package edu.vandy.mooc.rssreader.services;


import android.content.Intent;
import android.os.IBinder;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ServiceTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.SmallTest;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.concurrent.TimeoutException;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.core.IsInstanceOf.any;
import static org.junit.Assert.assertThat;


/**
 * Example of Service Integration Test
 * <p/>
 * https://developer.android.com/training/testing/integration-testing/service-testing.html
 * http://developer.android.com/reference/android/support/test/rule/ServiceTestRule.html
 */
@RunWith(AndroidJUnit4.class)
@SmallTest
public class TestServiceTest {


    @Rule
    public final ServiceTestRule mServiceRule = new ServiceTestRule();


    @Test
    public void testWithStartedService() throws TimeoutException {
        mServiceRule.startService(
                new Intent(InstrumentationRegistry.getTargetContext(), TestService.class));
        //do something
    }


    @Test
    public void testWithBoundService() throws TimeoutException {
//        // Create the service Intent.
//        Intent serviceIntent =
//                new Intent(InstrumentationRegistry.getTargetContext(),
//                        TestService.class);
//
//        // Data can be passed to the service via the Intent.
////        serviceIntent.putExtra(LocalService.SEED_KEY, 42L);
//
//        // Bind the service and grab a reference to the binder.
//        //   IBinder binder = mServiceRule.bindService(serviceIntent);
//
//        IBinder binder = mServiceRule.bindService(
//                new Intent(InstrumentationRegistry.getTargetContext(), TestService.class));
//
//        // Get the reference to the service, or you can call
//        // public methods on the binder directly.
////        TestService service =
//        TestService service =
//                ((TestService.LocalBinder) binder).getService();
//
//
//        // Verify that the service is working correctly.
//        assertThat(service.getRandomInt(), is(any(Integer.class)));
    }
}
