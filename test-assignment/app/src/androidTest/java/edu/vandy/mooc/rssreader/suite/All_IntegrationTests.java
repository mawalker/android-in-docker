package edu.vandy.mooc.rssreader.suite;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

// Runs all integration tests.
@RunWith(Suite.class)
@Suite.SuiteClasses({
        // comma separated list of classes that are test suites/integration tests.
        ServiceTests.class
})
public class All_IntegrationTests {
}
