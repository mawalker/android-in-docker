package edu.vandy.mooc.rssreader.logic;

/**
 * Created by Mike on 5/2/2016.
 */
public class MathOpsExample {

    public static int add(int a, int b) {
        return a + b;
    }

    public static int subtract(int a, int b) {
        return a - b;
    }

    public static int multiply(int a, int b) {
        return a * b;
    }

}
