package edu.vandy.mooc.rssreader.ui;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;

import java.lang.ref.WeakReference;

import edu.vandy.mooc.rssreader.R;

/**
 * Created by Mike on 5/2/2016.
 */
public class ExampleContextUse {

    private WeakReference<Context> mContext;

    public ExampleContextUse(Context context){
        mContext = new WeakReference<Context>(context);
    }

    String getAccountNameString(){
        return mContext.get().getString(R.string.ACCOUNT_NAME);
    }

}
