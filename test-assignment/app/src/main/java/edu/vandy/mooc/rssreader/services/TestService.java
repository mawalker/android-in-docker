package edu.vandy.mooc.rssreader.services;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;

import java.util.Random;

/**
 * TODO comments for random test service
 */
public class TestService extends Service {

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    // Random number generator
    private final Random mGenerator = new Random();

    // Binder given to clients
    private final IBinder mBinder = new LocalBinder();

    /**
     * method for clients
     */
    public int getRandomInt() {
        return mGenerator.nextInt(100);
    }

    public class LocalBinder extends Binder {

        public TestService getService() {
            // Return this instance of LocalService so clients can call public methods.
            return TestService.this;
        }
    }

}
