// ContentServiceAsync.aidl
package edu.vandy.mooc.rssreader.services;

// Declare any non-default types here with import statements
//import edu.vandy.mooc.rssreader.orm.Channel;
//import edu.vandy.mooc.rssreader.orm.Item;

import edu.vandy.mooc.rssreader.services.ContentServiceCallback;

interface ContentServiceAsync {

    oneway void registerCallback(in ContentServiceCallback callback);

    oneway void queryItem(
                        in String[] projection,
                        in String selection,
                        in String[] selectionArgs,
                        in String sortOrder
                        );

    oneway void queryChannel(
                        in String[] projection,
                        in String selection,
                        in String[] selectionArgs,
                        in String sortOrder
                        );


    oneway void queryItemWithCallback(
                        in ContentServiceCallback callback,
                        in String[] projection,
                        in String selection,
                        in String[] selectionArgs,
                        in String sortOrder
                        );

    oneway void queryChannelWithCallback(
                        in ContentServiceCallback callback,
                        in String[] projection,
                        in String selection,
                        in String[] selectionArgs,
                        in String sortOrder
                        );


}
