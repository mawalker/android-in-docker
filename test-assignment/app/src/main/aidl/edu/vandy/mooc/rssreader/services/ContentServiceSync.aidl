package edu.vandy.mooc.rssreader.services;

//import edu.vandy.mooc.rssreader.orm.Item;

interface ContentServiceSync {

    int getInt();

    List<String> queryItem(
                        in List<String> projection,
                        in String selection,
                        in List<String> selectionArgs,
                        in String sortOrder
                            );

}