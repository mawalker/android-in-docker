// ContentServiceCallback.aidl
package edu.vandy.mooc.rssreader.services;

// Declare any non-default types here with import statements
//import edu.vandy.mooc.rssreader.orm.Channel;
//import edu.vandy.mooc.rssreader.orm.Item;

interface ContentServiceCallback {

    oneway void getInt2(in int result);

    oneway void getItem(in int item);

 //   oneway void queryItemsResult(in List<Item> results);

 //   oneway void queryChannelResult(List<String> results);

}
