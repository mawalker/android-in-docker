package edu.vandy.mooc.rssreader.suite;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import edu.vandy.mooc.rssreader.logic.MathOpsExampleTest;

// Runs all unit tests.
@RunWith(Suite.class)
@Suite.SuiteClasses({
        //  comma separated list of classes that are test suites/unit tests.
        MathOpsExampleTest.class
})
public class All_UnitTests {
}
