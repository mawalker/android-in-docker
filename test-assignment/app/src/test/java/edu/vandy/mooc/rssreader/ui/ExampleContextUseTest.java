package edu.vandy.mooc.rssreader.ui;

import android.content.Context;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import edu.vandy.mooc.rssreader.R;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.when;

// Import the R associated with this app.

@RunWith(MockitoJUnitRunner.class)
public class ExampleContextUseTest {

    private static final String FAKE_STRING = "HELLO WORLD!";

    // The object(s) to mock, with '@Mock' annotation.
    @Mock
    Context mMockContext;

    // stand
    @Test
    public void readStringFromContext_LocalizedString() {
        // setup the mock <condition, thenReturn> pairs
        when(mMockContext.getString(R.string.ACCOUNT_NAME)).thenReturn(FAKE_STRING);

        // create object to test, with the mocked context
        ExampleContextUse myObjectUnderTest = new ExampleContextUse(mMockContext);

        // make the call(s) that
        String result = myObjectUnderTest.getAccountNameString();

        // do assertions (via hamcrest syntax)
        assertThat(result, is(FAKE_STRING));
    }

}
