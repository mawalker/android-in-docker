package edu.vandy.mooc.rssreader.logic;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class MathOpsExampleTest {

    static final private int mIntA = 5;
    static final private int mIntB = 2;
    static final private int mAaddB = 7;
    static final private int mAminusB = 3;
    static final private int mAtimeB = 10;

    @Test
    public void testAdd() {
        int rValue = MathOpsExample.add(mIntA, mIntB);
        assertThat(rValue, is(mAaddB));
    }

    @Test
    public void testSubtract() {
        int rValue = MathOpsExample.subtract(mIntA, mIntB);
        assertThat(rValue, is(mAminusB));
    }

    @Test
    public void testMultiply() {
        int rValue = MathOpsExample.multiply(mIntA, mIntB);
        assertThat(rValue, is(mAtimeB));
    }

}
