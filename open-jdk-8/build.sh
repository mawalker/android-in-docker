#!/bin/bash
###############################################################################
#
# This Scipt builds the Dockerfile for 'RighteousCoder/openJDK-8'
#
# It logs the start and stop time and stdout & stderr into log.txt
#
###############################################################################

# Define a timestamp function
timestamp() {
  date +"%T"
}

#####################################################

cd "${0%/*}"

log="log.txt"

timestamp | tee -a $log # print timestamp to screen & log

docker build -t righteouscoder/open-jdk-8 . 2>&1 | tee -a $log

timestamp | tee -a $log # print timestamp to screen & log
