Run ./build-23-sdk.sh then ./build-23-avd.sh then build image for assignment itself

README.txt                 - THIS FILE

build-23-sdk.sh            - Build the image for SDK-23 with the parameters in the script.
build-23-avd.sh            - Build the image for AVD-23 with the parameters in the script.

static-build-sdk-all.sh    - This script is able to build all (recent? at least 23/24, I think) combinations of SDK with command line params given
static-build-avd-23.sh     - This builds the AVD image for AVD-23, because Docker doesn't allow parameterized 'FROM' statements, so each needs its own dockerfile :/

static-dockerfile-avd-23   - Dockerfile to build the AVD to match the SDK-23 parameters
static-dockerfile-sdk-all  - Dockerfile to build all SDK image combinations

wait_for_emulator-start.sh - Script to run  

