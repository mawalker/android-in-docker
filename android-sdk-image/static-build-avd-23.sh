#!/bin/bash
###############################################################################
#
# This Scipt builds the empty Docker Images that have the target Android SDK
#    and other related stuff
#
# It logs the start and stop time and stdout & stderr into the log file
#
# @Author: Michael A. Walker
#
# @Date 2016-08-09
#
# Version 1.0:
#       - Fully Commented script
#       - Added support for Parameters:
#            (not full --option=value style, just 0, 3, or 4 string parems)
#            0 Parems:   Just use the API 24 Defaults.
#            1-3 Parems: The 3 build-arguments (all 3 Required & in exact order)
#            4th Parem:  File for log output. (optional)
#
###############################################################################

# Define a re-usalbe timestamp function.
#     Uses  _international standard_  date, time, and timezone formates
#     seperated by an underscore between each(for easy regex parsing, etc.) 
#     If curious see: http://fits.gsfc.nasa.gov/iso-time.html
timestamp() {
  date +"%Y-%M-%d_%T_%z"
}

###############################################################################
#
# Defaults are declared here.
#
###############################################################################

# Default log file name.
DEFAULT_LOGFILE_NAME="build-android-image-log.txt"

# Default SDK version to download.
ANDROID_SDK_TAR_VER_VALUE=24.4.1

# Default Build-Tools version.
BUILD_TOOLS_VER_VALUE=24.0.1

# Default target platform
ANDROID_PLATFORM_VER_VALUE=android-24

# Default target system image
SYSTEM_IMAGE_TAG=sys-img-armeabi-v7a-android-24

# Set -log- default log file.
log=$DEFAULT_LOGFILE_NAME

# Declare acceptable parameter range.
MIN_PARAMS=4 # the 3 -build-arg-(s) for the image.
MAX_PARAMS=5 # only if different log file is given.

###############################################################################
#
# Handle Command-Line Paremeters (if any) Only acceptable #(s) are: 0, 4, or 5.
#
###############################################################################

# Check if there are any paremeters
if [[ $# -ne 0 ]]; then
   # Check if provided paremeters are not in the  correct range.
   if  [[ $# -lt "$MIN_PARAMS" ]] || [[ $# -gt "MAX_PARAMS" ]]; then
      #########################################################################
      # NOT in the correct range. 
      #########################################################################
      echo # Print the following Error Mesage and Guide
      # echo -n doesn't add EOL character at end, (keeps next line < 80 chars)
      echo -n "This script needs exactly 0, " $MIN_PARAMS ", or "
      echo $MAX_PARAMS " arguemnts exactly."
      echo
      echo "those paremeters are (in exact order): "
      echo "n/a) No Paremeters and accept defaults. OR... "
      echo "1) Android Linux SDK version number to download."
      echo "2) Build-Tools version to use."
      echo "3) Target Platform(from \'Android list sdk -a -e\'."
      echo "4) System Image to use (from \'Android list sdk -a -e\'."
      echo "5) File to save the log to (optional)."
      exit 1 # Exit the script after
   else
      #########################################################################
      # IS in the correct range.
      #########################################################################
      # Overwrite the Default of the variable with the Paremeter (if it exists)
      PARAM_1=$1
      PARAM_2=$2
      PARAM_3=$3
      PARAM_4=$4
      PARAM_5=$5
      # These above 4 variables will be -null- if the param wasn not given.
      if [[ -n $PARAM_1 ]]; then
         ANDROID_SDK_TAR_VER_VALUE=$PARAM_1
      fi
      if [[ -n $PARAM_2 ]]; then
         BUILD_TOOLS_VER_VALUE=$PARAM_2
      fi
      if [[ -n $PARAM_3 ]]; then
         ANDROID_PLATFORM_VER_VALUE=$PARAM_3
      fi
      if [[ -n $PARAM_4 ]]; then
         SYSTEM_IMAGE_TAG=$PARAM_4
      fi
      if [[ -n $PARAM_5 ]]; then
         DEFAULT_LOGFILE_NAME=$PARAM_5
      fi
   fi # End of -else- (meaning correct # of paremeters 3, or 4)
fi # End (if there were paremeters at all)

###############################################################################
#
# Actual Body of Script Now.
#
# log+times-stamp the docker image creation, from start to finish.
#
###############################################################################

# Remove the old log-file if it exists.
rm -rf $log

# Use a single TimeStamp to allow -exact- same value to be used in logging too
TIMESTAMP_VALUE=$(timestamp)

# Start logging w/ timestamp value.
echo $TIMESTAMP_VALUE | tee -a $log # print timestamp to screen & log

# Key command to actually build the image with the arguements.
#    stdout and stderr both redirected to logfile.
# IMPORTANT: can NOT put commenft lines after lines that end with [ \ ].
docker build \
  --build-arg ANDROID_SDK_TAR_VER=$ANDROID_SDK_TAR_VER_VALUE \
  --build-arg BUILD_TOOLS_VER=$BUILD_TOOLS_VER_VALUE \
  --build-arg ANDROID_PLATFORM_VER=$ANDROID_PLATFORM_VER_VALUE \
  --build-arg SYSTEM_IMAGE_TAG=$SYSTEM_IMAGE_TAG \
  --build-arg TIMESTAMP=$TIMESTAMP_VALUE \
  -f ./static-dockerfile-avd-23 \
  -t righteouscoder/android-sdk-r$BUILD_TOOLS_VER_VALUE-$ANDROID_PLATFORM_VER_VALUE-avd . 2>&1 | tee -a $log


# Print final timestamp to screen & log file.
timestamp | tee -a $log
