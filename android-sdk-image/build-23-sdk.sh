#!/bin/bash
###############################################################################
#
# This Scipt is a helper/config scipt for building Android SDK Docker images.
#
# @Author: Michael A. Walker
# @Date 2016-08-09
#
###############################################################################

###############################################################################
# Define the 3 build-argument parameters
###############################################################################

cd "${0%/*}"

# The specific file revision(unique part) of the Linux Android SDK to download.
SDK_FILE_VER=24.4.1

# This is the unique part of the build-tools revision to download and install.
BUILD_TOOLS_VER=23.0.3

# This is the target -Android SDK Platform- to download and install.
PLATFORM_VER=android-23

###############################################################################
# Call the full image creation script (w/ new  paremeters.)
###############################################################################

echo "****************************************************"
echo "** BUILDING IMAGES WITH THE FOLLOWING PARAMETERS: **"
echo "****************************************************"
echo "SDK_FILE_VER= "$SDK_FILE_VER
echo "BUILD_TOOLS_VER="$BUILD_TOOLS_VER
echo "PLATFORM_VER="$PLATFORM_VER
echo "****************************************************"

./static-build-sdk-all.sh   $SDK_FILE_VER  $BUILD_TOOLS_VER  $PLATFORM_VER
