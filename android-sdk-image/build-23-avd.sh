#!/bin/bash
###############################################################################
#
# This Scipt is a helper/config scipt for building Android SDK Docker images.
#
# This script just has the build-arguments as paremeters to the static
#   [ create-android-image.sh ] script.
#   (That script is what does the actual parameterized image building process)
#
# @Author: Michael A. Walker
#
# @Date 2016-08-09
#
# Version 1.0:
#      - Created this file
#      - Added the build-arguement parameter values.
#      - Added call to (now paremeterized, but otherwise static) build script.
#      - Added comments
#
###############################################################################

##########################################
# Define the 3 build-argument parameters
##########################################

cd "${0%/*}"

# The specific file revision(unique part) of the Linux Android SDK to download.
# Example SDK URL: https://dl.google.com/android/android-sdk_r24.4.1-linux.tgz
SDK_FILE_VER=24.4.1

# This is the unique part of the build-tools revision to download and install.
# Example being: [ build-tools-24.0.1 ] w/ format [ build-tools-##.##.## ].
BUILD_TOOLS_VER=23.0.3

# This is the target -Android SDK Platform- to download and install.
# Desc: Android SDK Platform 24 (Revision 2)
# I'm not sure how different revisions are handled here...
# I believe [ android-24 ] will end up just getting the latest revision.
PLATFORM_VER=android-23

SYSTEM_IMAGE_TAG=sys-img-armeabi-v7a-android-23

##########################################
# Call the full image creation script
##########################################
./static-build-avd-23.sh  $SDK_FILE_VER  $BUILD_TOOLS_VER  $PLATFORM_VER  $SYSTEM_IMAGE_TAG
#echo  $SDK_FILE_VER  $BUILD_TOOLS_VER  $PLATFORM_VER  $AVD_IMAGE

