Run [ ./build-precourser-images.sh* ] then:

1) create a container of that instance that loads the files from /test-assignment/*
2) once inside the container run [ ./start-avd.sh ] and then [ wait_for_emulator-start.sh* ] before proceeding
3) run [ ./gradlew clean test cAT ] to run the unit tests and then the integration tests.

the installation of the debug.apk stalls(almost always) and is unpredictable. 